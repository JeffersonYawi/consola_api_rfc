﻿using System.Collections.Generic;
using System.Data;

namespace Model.bean
{
    public class BECorreo
    {
        public List<string[]> direcciones { get; set; }
        public List<string[]> direccionesCopia { get; set; }
        public DataSet tabla { get; set; }
        public int resultado { get; set; }
        public string mensaje { get; set; }
    }
}
