﻿using System;
using Model.bean;
using System.Data.SqlClient;
using System.Data;
using Model.functions;
using System.Collections;
using System.Collections.Generic;

namespace Model
{
    public class ConsultaModel
    {
        public static BECorreo ObtenerCorreo(string grupo, string tipo, string servicio)
        {
            BECorreo objCorreo = new BECorreo();

            try
            {
                ArrayList alParameters = new ArrayList();
                SqlParameter parameter = new SqlParameter("@Grupo", SqlDbType.VarChar, 10);
                parameter.Value = grupo;
                alParameters.Add(parameter);

                parameter = new SqlParameter("@Tipo", SqlDbType.VarChar, 20);
                parameter.Value = tipo;
                alParameters.Add(parameter);

                parameter = new SqlParameter("@Servicio", SqlDbType.VarChar, 20);
                parameter.Value = servicio;
                alParameters.Add(parameter);

                DataTable dt = SqlConnector.getDataTable("[dbo].[usp_liquidacion_listado_correos]", alParameters);

                if (dt.Rows.Count > 1)
                {
                    objCorreo.direcciones = new List<string[]>();
                    objCorreo.direccionesCopia = new List<string[]>();
                    foreach (DataRow row in dt.Rows)
                    {
                        if (!String.IsNullOrEmpty(row[0].ToString()))
                        {
                            string[] direccion = new string[2];
                            direccion[0] = row[0].ToString();
                            direccion[1] = row[1].ToString();
                            if (int.Parse(row[2].ToString()) > 2)
                                objCorreo.direccionesCopia.Add(direccion);
                            else
                                objCorreo.direcciones.Add(direccion);
                        }
                    }

                    alParameters = new ArrayList();
                    parameter = new SqlParameter("@Grupo", SqlDbType.VarChar, 10);
                    parameter.Value = grupo;
                    alParameters.Add(parameter);

                    parameter = new SqlParameter("@Tipo", SqlDbType.VarChar, 20);
                    parameter.Value = tipo;
                    alParameters.Add(parameter);

                    parameter = new SqlParameter("@Servicio", SqlDbType.VarChar, 20);
                    parameter.Value = servicio;
                    alParameters.Add(parameter);

                    DataSet ds = SqlConnector.getDataset("[dbo].[usp_liquidacion_servicios]", alParameters);
                    if (ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0)
                    {
                        objCorreo.tabla = ds;
                        objCorreo.resultado = 0;
                        objCorreo.mensaje = "EL CORREO SE OBTUVO CORRECTAMENTE";
                    }
                    else
                    {
                        objCorreo.resultado = 2;
                        objCorreo.mensaje = "EL CORREO NO TIENE LIQUIDACIONES";
                    }
                }
                else
                {
                    objCorreo.resultado = 1;
                    objCorreo.mensaje = "EL CORREO NO TUVO DESTINATARIOS";
                }
            }
            catch (Exception ex)
            {
                objCorreo.resultado = 3;
                objCorreo.mensaje = "EXECEPTION: " + ex.Message;
            }
            return objCorreo;
        }

        public static string[] ObtenerDatosDeLiquidacion()
        {
            string[] datos = new string[5];

            try
            {
                ArrayList alParameters = new ArrayList();
                SqlParameter parameter = new SqlParameter("@Grupo", SqlDbType.VarChar, 10);
                parameter.Value = "HFT";
                alParameters.Add(parameter);

                parameter = new SqlParameter("@Servicio", SqlDbType.VarChar, 20);
                //parameter.Value = "PROPIOS";
                parameter.Value = "TERCEROS";
                alParameters.Add(parameter);

                DataTable dt = SqlConnector.getDataTable("[dbo].[usp_liquidacion_verificar_envio]", alParameters);

                if (dt.Rows.Count == 1)
                {
                    datos[0] = dt.Rows[0][0].ToString();
                    datos[1] = dt.Rows[0][1].ToString();
                    datos[2] = dt.Rows[0][2].ToString();
                    datos[3] = dt.Rows[0][3].ToString();
                    datos[4] = dt.Rows[0][4].ToString();
                }
                else
                {
                    datos[0] = ""; datos[1] = ""; datos[2] = ""; datos[3] = ""; datos[4] = "";
                }
            }
            catch (Exception ex)
            {
                datos[0] = ""; datos[1] = ""; datos[2] = ""; datos[3] = ""; datos[4] = "";
            }

            return datos;
        }
    }
}
