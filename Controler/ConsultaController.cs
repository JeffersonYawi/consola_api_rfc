﻿using Model.bean;
using Model;

namespace Controler
{
    public class ConsultaController
    {
        public static BECorreo ObtenerCorreo(string grupo, string tipo, string servicio)
        {
            return ConsultaModel.ObtenerCorreo(grupo, tipo, servicio);
        }

        public static string[] ObtenerDatosDeLiquidacion()
        {
            return ConsultaModel.ObtenerDatosDeLiquidacion();
        }
    }
}
