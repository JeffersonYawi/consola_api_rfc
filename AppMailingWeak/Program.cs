﻿//using SAP.Middleware.Connector;
//using System;
//using System.Data;
//using Infrastructure.Utils;
//using System.Configuration;
//using System.Data.SqlClient;
//using System.Threading.Tasks;
//using System.Collections;
//using System.Threading;

//namespace AgrisysLiquidacionesCorreo
//{
//    public class Program {

//        public static void Main(string[] args)
//        {
//            syncData();
//        }        

//        public static void syncData()
//        {
//            string connectionString = ConfigurationManager.ConnectionStrings["conexionbd"].ConnectionString;

//            string destinationConfigName = "NCO_TESTS";
//            IDestinationConfiguration destinationConfig = new RfcConnector();
//            destinationConfig.GetParameters(destinationConfigName);
//            if (RfcDestinationManager.TryGetDestination(destinationConfigName) == null)
//            {
//                RfcDestinationManager.RegisterDestinationConfiguration(destinationConfig);
//            }
//            RfcDestination SapRfcDestination = RfcDestinationManager.GetDestination(destinationConfigName);
//            RfcCustomDestination _customDestination = SapRfcDestination.CreateCustomDestination();            
//            //Consulta Fecha Actual y Restada 90 días

//            string connectionString = ConfigurationManager.ConnectionStrings["conexionbd"].ConnectionString;
//            DataTable dtFechas = SqlConnector.getConsultaDataTable(connectionString, "[dbo].[sap_obtener_fechas_FI_INGRESOS]");
//            DataTable dtFechas2 = SqlConnector.getConsultaDataTable(connectionString, "[dbo].[sap_obtener_fechas_FI_COBRANZAS]");

//            foreach (DataRow drow2 in dtFechas.Rows){
//                string anioActual = drow2[0].ToString();
//                string mesActual = drow2[1].ToString();
//                string diaActual = drow2[2].ToString();
//                string anioRestado = drow2[3].ToString();
//                string mesRestado = drow2[4].ToString();
//                string diaRestado = drow2[5].ToString();
//                string fechaActual = anioActual + mesActual + diaActual;
//                string fechaRestado = anioRestado + mesRestado + diaRestado;

//                //envia parametros fechas
//                ArrayList parameters = new ArrayList();
//                SqlParameter parameter;

//                var fecha1 = anioRestado + '-' + mesRestado + '-' + diaRestado;
//                var fecha2 = anioActual + '-' + mesActual + '-' + diaActual;

//                parameter = new SqlParameter("@fecha1", SqlDbType.VarChar, 20);
//                parameter.Value = fecha1;
//                parameters.Add(parameter);

//                parameter = new SqlParameter("@fecha2", SqlDbType.VarChar, 20);
//                parameter.Value = fecha2;
//                parameters.Add(parameter);

//                DataTable dtElimina = SqlConnector.getEliminaDataTable(connectionString, "[dbo].[sap_depura_fechas_FI_INGRESOS]", parameters);


//                try
//                {
//                    IRfcFunction ObjRFC = _customDestination.Repository.CreateFunction("ZFMFI_FBL3N_INGRESOS");

//                    IRfcTable rfc_tbl_PT_BUKRS = ObjRFC.GetTable("PT_BUKRS");
//                    RfcStructureMetadata metaData1 = _customDestination.Repository.GetStructureMetadata("ACC_S_RA_BUKRS");

//                    IRfcTable rfc_tbl_PT_BUDAT = ObjRFC.GetTable("PT_BUDAT");
//                    RfcStructureMetadata metaData2 = _customDestination.Repository.GetStructureMetadata("FAGL_RANGE_BUDAT");

//                    IRfcStructure structCC1 = metaData1.CreateStructure();
//                    IRfcStructure structCC2 = metaData2.CreateStructure();

//                    structCC1.SetValue("SIGN", "I");
//                    structCC1.SetValue("OPTION", "BT");
//                    structCC1.SetValue("LOW", "1100");
//                    structCC1.SetValue("HIGH", "1500");

//                    structCC2.SetValue("SIGN", "I");
//                    structCC2.SetValue("OPTION", "BT");

//                    structCC2.SetValue("LOW", fecha1);
//                    structCC2.SetValue("HIGH", fecha2);

//                    rfc_tbl_PT_BUKRS.Append(structCC1);
//                    rfc_tbl_PT_BUDAT.Append(structCC2);




//                    ObjRFC.SetValue("PT_BUKRS", rfc_tbl_PT_BUKRS);
//                    ObjRFC.SetValue("PT_BUDAT", rfc_tbl_PT_BUDAT);





//                    IRfcTable ET_REP_INGRESOS = ObjRFC.GetTable("ET_REP_INGRESOS");

//                    DataTable table3 = new DataTable();

//                    table3.Columns.Add("BUKRS", typeof(string));
//                    table3.Columns.Add("NAME1", typeof(string));
//                    table3.Columns.Add("KONTO", typeof(string));
//                    table3.Columns.Add("ZUONR", typeof(string));
//                    table3.Columns.Add("XBLNR", typeof(string));
//                    table3.Columns.Add("BELNR", typeof(string));
//                    table3.Columns.Add("BLART", typeof(string));
//                    table3.Columns.Add("BLDAT", typeof(string));
//                    table3.Columns.Add("UMSKZ", typeof(string));
//                    table3.Columns.Add("BUDAT", typeof(string));
//                    table3.Columns.Add("ICO_DUE", typeof(string));
//                    table3.Columns.Add("FAEDT", typeof(string));
//                    table3.Columns.Add("WRSHB", typeof(string));
//                    table3.Columns.Add("WAERS", typeof(string));
//                    table3.Columns.Add("BWWRT", typeof(string));
//                    table3.Columns.Add("HWAER", typeof(string));
//                    table3.Columns.Add("AUGBL", typeof(string));
//                    table3.Columns.Add("HKONT", typeof(string));
//                    table3.Columns.Add("SGTXT", typeof(string));
//                    table3.Columns.Add("BSTAT", typeof(string));
//                    table3.Columns.Add("GJAHR", typeof(string));
//                    table3.Columns.Add("BWWR2", typeof(string));
//                    table3.Columns.Add("HWAE2", typeof(string));
//                    table3.Columns.Add("REBZG", typeof(string));
//                    table3.Columns.Add("BUTXT", typeof(string));
//                    table3.Columns.Add("CONCATENACION", typeof(string));
//                    table3.Columns.Add("CTA_BANC", typeof(string));
//                    table3.Columns.Add("BANKA", typeof(string));
//                    table3.Columns.Add("ANO_FC", typeof(string));
//                    table3.Columns.Add("MES_FC", typeof(string));
//                    table3.Columns.Add("COD", typeof(string));
//                    table3.Columns.Add("SEMANA", typeof(string));
//                    table3.Columns.Add("TIPO_INGRESO", typeof(string));
//                    table3.Columns.Add("HBKID", typeof(string));
//                    table3.Columns.Add("HKTID", typeof(string));
//                    table3.Columns.Add("ZDMBE2", typeof(string));
//                    table3.Columns.Add("WAERS_USD", typeof(string));



//                    ObjRFC.Invoke(SapRfcDestination);

//                    table3 = GlobalData.generateDataTable(table3, ET_REP_INGRESOS);

//                   if (table3.Rows.Count > 0)
//                    {                                
//                        string message = SqlConnector.publicDataTable(connectionString, table3, "AGRI_RPT_FI_INGRESOS");
//                        Console.WriteLine(message);
//                    }
//                    else
//                    {
//                        Console.WriteLine("No hay data en la tabla.");
//                        Thread.Sleep(5000);
//                    }

//                    Console.WriteLine("El programa ha finalizado.");
//                    Thread.Sleep(5000);
//                }
//                catch (Exception ex)
//                {
//                    Console.WriteLine(ex.Message);
//                }
//            }

//            foreach (DataRow drow3 in dtFechas2.Rows)
//            {
//                string anioActual = drow3[0].ToString();
//                string mesActual = drow3[1].ToString();
//                string diaActual = drow3[2].ToString();
//                string anioRestado = drow3[3].ToString();
//                string mesRestado = drow3[4].ToString();
//                string diaRestado = drow3[5].ToString();
//                string fechaActual = anioActual + mesActual + diaActual;
//                string fechaRestado = anioRestado + mesRestado + diaRestado;

//                //envia parametros fechas
//                ArrayList parameters = new ArrayList();
//                SqlParameter parameter;

//                var fecha3 = anioRestado + '-' + mesRestado + '-' + diaRestado;
//                var fecha4 = anioActual + '-' + mesActual + '-' + diaActual;

//                parameter = new SqlParameter("@fecha1", SqlDbType.VarChar, 20);
//                parameter.Value = fecha3; // 27-10-22
//                parameters.Add(parameter);

//                parameter = new SqlParameter("@fecha2", SqlDbType.VarChar, 20);
//                parameter.Value = fecha4;
//                parameters.Add(parameter); // 25-10-22

//                DataTable dtElimina = SqlConnector.getEliminaDataTable(connectionString, "[dbo].[sap_depura_fechas_FI_COBRANZAS]", parameters);

//                try
//                {

//                    DateTime fechaInicio = Convert.ToDateTime(fecha3);
//                    DateTime fechaFin = Convert.ToDateTime(fecha4);

//                    for (int i = 0; fechaInicio <= fechaFin; i++)
//                    {
//                        // API RFC DONDE SE CONSUME
//                        IRfcFunction ObjRFC = _customDestination.Repository.CreateFunction("ZFMFI_FBL5N_COBRANZA");

//                        // CSE CREA EL PARAMETRO DE TIPO TABLA QUE SE ENVIARA AL RFC
//                        IRfcTable rfc_tbl_PT_BUKRS = ObjRFC.GetTable("PT_BUKRS");


//                        // EL "ACC_S_RA_BUKRS" SON LAS METADATAS QUE TE DEVUELVE EL API
//                        RfcStructureMetadata metaData1 = _customDestination.Repository.GetStructureMetadata("ACC_S_RA_BUKRS");
//                        // CREAMO ES STRUCTURE QUE PERMITIRA INGRESAR LOS PARAMETROS
//                        IRfcStructure structCC1 = metaData1.CreateStructure();

//                        // ENVIAMOS LO PARAMETROS
//                        structCC1.SetValue("SIGN", "I");
//                        structCC1.SetValue("OPTION", "BT");
//                        structCC1.SetValue("LOW", "1100");
//                        structCC1.SetValue("HIGH", "1500");

//                        // AÑADIMOS NUESTRO STRUCTURE A LA TABLE
//                        rfc_tbl_PT_BUKRS.Append(structCC1);

//                        // AÑADIMOS NUESTRAS TABLA PARA COMO MANDARLA COMO VALOR DE LA KEY QUE SOLICITAN
//                        ObjRFC.SetValue("PT_BUKRS", rfc_tbl_PT_BUKRS);

//                        // EL PT_STIDA ES EL CAMPO QUE DEBE DE RECORRER EL RANGO DE FECHA ENVIADOS 
//                        fechaInicio = fechaInicio.AddDays(i);
//                        string fechaInicioParse = fechaInicio.ToString("yyyy-MM-dd");
//                        ObjRFC.SetValue("P_STIDA", fechaInicioParse);


//                        // TABLA QUE OBTENDRA LA RESPUESTA
//                        IRfcTable ET_REP_COBRANZA = ObjRFC.GetTable("ET_REP_COBRANZA");

//                        DataTable table4 = new DataTable();

//                        table4.Columns.Add("BUKRS", typeof(string));
//                        table4.Columns.Add("NAME1", typeof(string));
//                        table4.Columns.Add("KONTO", typeof(string));
//                        table4.Columns.Add("ZUONR", typeof(string));
//                        table4.Columns.Add("XBLNR", typeof(string));
//                        table4.Columns.Add("BELNR", typeof(string));
//                        table4.Columns.Add("BLART", typeof(string));
//                        table4.Columns.Add("BLDAT", typeof(string));
//                        table4.Columns.Add("UMSKZ", typeof(string));
//                        table4.Columns.Add("BUDAT", typeof(string));
//                        table4.Columns.Add("XDUEN", typeof(string));
//                        table4.Columns.Add("FAEDT", typeof(string));
//                        table4.Columns.Add("WRSHB", typeof(string));
//                        table4.Columns.Add("WAERS", typeof(string));
//                        table4.Columns.Add("BWWRT", typeof(string));
//                        table4.Columns.Add("HWAER", typeof(string));
//                        table4.Columns.Add("AUGBL", typeof(string));
//                        table4.Columns.Add("HKONT", typeof(string));
//                        table4.Columns.Add("SGTXT", typeof(string));
//                        table4.Columns.Add("BSTAT", typeof(string));
//                        table4.Columns.Add("GJAHR", typeof(string));
//                        table4.Columns.Add("BWWR2", typeof(string));
//                        table4.Columns.Add("HWAE2", typeof(string));
//                        table4.Columns.Add("REBZG", typeof(string));
//                        table4.Columns.Add("PRCTR", typeof(string));
//                        table4.Columns.Add("LTEXT", typeof(string));
//                        table4.Columns.Add("ZZ_TIPTRA", typeof(string));
//                        table4.Columns.Add("ZZ_TIPTRA_TXT", typeof(string));
//                        table4.Columns.Add("VBEL2", typeof(string));
//                        table4.Columns.Add("VBELN", typeof(string));
//                        table4.Columns.Add("DIAS", typeof(string));
//                        table4.Columns.Add("STATUS_VCTO", typeof(string));
//                        table4.Columns.Add("STATUS", typeof(string));
//                        table4.Columns.Add("ANO_FD", typeof(string));
//                        table4.Columns.Add("MES_FD", typeof(string));
//                        table4.Columns.Add("BUTXT", typeof(string));
//                        table4.Columns.Add("COD", typeof(string));
//                        table4.Columns.Add("TIPO_VENTA", typeof(string));
//                        table4.Columns.Add("RESPONSABLE", typeof(string));
//                        table4.Columns.Add("ANTICUAMIENTO", typeof(string));
//                        table4.Columns.Add("VKGRP", typeof(string));
//                        table4.Columns.Add("VENDEDOR", typeof(string));
//                        table4.Columns.Add("ZDMBE2", typeof(string));
//                        table4.Columns.Add("WAERS_USD", typeof(string));



//                        ObjRFC.Invoke(SapRfcDestination);

//                        table4 = GlobalData.generateDataTable(table4, ET_REP_COBRANZA);


//                        if (table4.Rows.Count > 0)
//                        {
//                            string message = SqlConnector.publicDataTable(connectionString, table4, "AGRI_RPT_FI_COBRANZAS");
//                            Console.WriteLine(message);
//                        }
//                        else
//                        {
//                            Console.WriteLine("No hay data en la tabla.");
//                            Thread.Sleep(5000);
//                        }
//                        //if (table2.Rows.Count > 0)
//                        //{
//                        //    string message = SqlConnector.publicDataTable(connectionString, table2, "dbo.AGRI_RPT_KOB1");
//                        //    Console.WriteLine(message);
//                        //}
//                        //else
//                        //{
//                        //    Console.WriteLine("No hay data en la tabla.");
//                        //    Thread.Sleep(5000);
//                        //}
//                        Console.WriteLine("El programa ha finalizado.");
//                        Thread.Sleep(5000);
//                    }
//                }
//                catch (Exception ex)
//                {
//                    Console.WriteLine(ex.Message);
//                }
//            }

//        }
//    }
//}

using Fbl3n;
using Infrastructure.Utils;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static async Task Main()
    {
        //URL APIS
        string ApiFBL3N = ConfigurationManager.AppSettings["ApiFBL3N"];
        string ApiFBL5N = ConfigurationManager.AppSettings["ApiFBL5N"];
        string ApiZSD004 = ConfigurationManager.AppSettings["ApiZSD004"];

        string ApiCJI3 = ConfigurationManager.AppSettings["ApiCJI3"];
        string ApiIW38_KOB1 = ConfigurationManager.AppSettings["ApiIW38_KOB1"];


        string connectionString = ConfigurationManager.ConnectionStrings["conexionbd"].ConnectionString;
        //ApiFBL3N
        DataTable dtFechas = SqlConnector.getConsultaDataTable(connectionString, "[dbo].[sap_obtener_fechas_FI_INGRESOS]");

        //ApiFBL5N
        DataTable dtFechas2 = SqlConnector.getConsultaDataTable(connectionString, "[dbo].[sap_obtener_fechas_FI_COBRANZAS]");

        //ApiZSD004
        DataTable dtFechas3 = SqlConnector.getConsultaDataTable(connectionString, "[dbo].[sap_obtener_fechas_ZSD004]");

        //ApiCJI3
        DataTable dtProyectos1 = SqlConnector.getConsultaDataTable(connectionString, "[dbo].[sap_proyectos]");
        DataTable dtFechas4 = SqlConnector.getConsultaDataTable(connectionString, "[dbo].[sap_obtener_fechas]");

        //ApiIW38_KOB1

        DataTable dtFechas5 = SqlConnector.getConsultaDataTable(connectionString, "[iw38_y_kob1].[sap_obtener_fechas_iw38_y_kob1]");


        //FI_INGRESOS

        foreach (DataRow drow in dtFechas.Rows)
        {

            string anioActual = drow[0].ToString();
            string mesActual = drow[1].ToString();
            string diaActual = drow[2].ToString();
            string anioRestado = drow[3].ToString();
            string mesRestado = drow[4].ToString();
            string diaRestado = drow[5].ToString();
            string fechaActual = anioActual + mesActual + diaActual;
            string fechaRestado = anioRestado + mesRestado + diaRestado;


            //envia parametros fechas
            ArrayList parameters = new ArrayList();
            SqlParameter parameter;

            var fecha1 = anioRestado + '-' + mesRestado + '-' + diaRestado;
            var fecha2 = anioActual + '-' + mesActual + '-' + diaActual;

            parameter = new SqlParameter("@fecha1", SqlDbType.VarChar, 20);
            parameter.Value = fecha1;
            parameters.Add(parameter);

            parameter = new SqlParameter("@fecha2", SqlDbType.VarChar, 20);
            parameter.Value = fecha2;
            parameters.Add(parameter);

            DataTable dtElimina = SqlConnector.getEliminaDataTable(connectionString, "[dbo].[sap_depura_fechas_FI_INGRESOS]", parameters);

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    // Establecer el encabezado Content-Type a application/json
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string jsonParams = string.Format(@"{{
                         ""TipoExec"": {{
                             ""SIGN"": ""I"",
                             ""OPTION"": ""BT"",
                             ""LOW"": ""{0}"",
                             ""HIGH"": ""{1}""
                         }}
                     }}", fechaRestado, fechaActual);


                    HttpResponseMessage response = await client.PostAsync(ApiFBL3N, new StringContent(jsonParams, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(responseBody))
                        {
                            Fbi3nResponse rootObject = JsonConvert.DeserializeObject<Fbi3nResponse>(responseBody);


                            if (rootObject != null && rootObject.tablaApi != null && rootObject.tablaApi.Length > 0)
                            {
                                //DataTable dataTable = ConvertToDataTable(rootObject.tablaApi);
                                DataTable dataTable = DataTableConverter.ConvertToDataTable(rootObject.tablaApi);

                                string message = SqlConnector.publicDataTable(connectionString, dataTable, "AGRI_RPT_FI_INGRESOS");
                                Console.WriteLine("Inserción exitosa en la tabla AGRI_RPT_FI_INGRESOS.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("La respuesta de la API no contiene datos.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("La solicitud no fue exitosa. Código de estado: " + response.StatusCode);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error al realizar la solicitud a la API: " + ex.Message);
                }
            }
        }

        //sap_depura_fechas_FI_COBRANZAS

        foreach (DataRow drow3 in dtFechas2.Rows)
        {
            string anioActual = drow3[0].ToString();
            string mesActual = drow3[1].ToString();
            string diaActual = drow3[2].ToString();
            string anioRestado = drow3[3].ToString();
            string mesRestado = drow3[4].ToString();
            string diaRestado = drow3[5].ToString();
            string fechaActual = anioActual + mesActual + diaActual;
            string fechaRestado = anioRestado + mesRestado + diaRestado;

            //envia parametros fechas
            ArrayList parameters = new ArrayList();
            SqlParameter parameter;

            var fecha3 = anioRestado + '-' + mesRestado + '-' + diaRestado;
            var fecha4 = anioActual + '-' + mesActual + '-' + diaActual;

            parameter = new SqlParameter("@fecha1", SqlDbType.VarChar, 20);
            parameter.Value = fecha3; // 27-10-22
            parameters.Add(parameter);

            parameter = new SqlParameter("@fecha2", SqlDbType.VarChar, 20);
            parameter.Value = fecha4;
            parameters.Add(parameter); // 25-10-22

            DataTable dtElimina = SqlConnector.getEliminaDataTable(connectionString, "[dbo].[sap_depura_fechas_FI_COBRANZAS]", parameters);


            using (HttpClient client = new HttpClient())
            {
                try
                {
                    // Establecer el encabezado Content-Type a application/json
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string jsonParams = string.Format(@"{{
                        ""TipoExec"": {{
                            ""LOW"": ""{0}"",
                            ""HIGH"": ""{1}""
                        }}
                    }}", fecha3, fecha4);


                    HttpResponseMessage response = await client.PostAsync(ApiFBL5N, new StringContent(jsonParams, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(responseBody))
                        {
                            Fbi5nResponse rootObject = JsonConvert.DeserializeObject<Fbi5nResponse>(responseBody);


                            if (rootObject != null && rootObject.tablaApi != null && rootObject.tablaApi.Length > 0)
                            {
                                //DataTable dataTable = ConvertToDataTable(rootObject.tablaApi);
                                DataTable dataTable = DataTableConverter.ConvertToDataTable(rootObject.tablaApi);

                                string message = SqlConnector.publicDataTable(connectionString, dataTable, "AGRI_RPT_FI_COBRANZAS");
                                Console.WriteLine("Inserción exitosa en la tabla AGRI_RPT_FI_COBRANZAS.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("La respuesta de la API no contiene datos.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("La solicitud no fue exitosa. Código de estado: " + response.StatusCode);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error al realizar la solicitud a la API: " + ex.Message);
                }
            }

        }

        //sap_depura_fechas_ZSD004

        foreach (DataRow drow in dtFechas3.Rows)
        {
            string anioActual = drow[0].ToString();
            string mesActual = drow[1].ToString();
            string diaActual = drow[2].ToString();
            string anioRestado = drow[3].ToString();
            string mesRestado = drow[4].ToString();
            string diaRestado = drow[5].ToString();
            string fechaActual = anioActual + mesActual + diaActual;
            string fechaRestado = anioRestado + mesRestado + diaRestado;

            //envia parametros fechas
            ArrayList parameters = new ArrayList();
            SqlParameter parameter;

            var fecha1 = anioRestado + '-' + mesRestado + '-' + diaRestado;
            var fecha2 = anioActual + '-' + mesActual + '-' + diaActual;

            parameter = new SqlParameter("@fecha1", SqlDbType.VarChar, 20);
            parameter.Value = fecha1;
            parameters.Add(parameter);

            parameter = new SqlParameter("@fecha2", SqlDbType.VarChar, 20);
            parameter.Value = fecha2;
            parameters.Add(parameter);

            DateTime fecha1Parse = DateTime.Parse(fecha1);
            DateTime fecha2Parse = DateTime.Parse(fecha2);

            DataTable dtElimina = SqlConnector.getEliminaDataTable(connectionString, "[dbo].[RPT_ZSD004]", parameters);

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    // Establecer el encabezado Content-Type a application/json
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = TimeSpan.FromMinutes(5);

                    while (fecha1Parse < fecha2Parse)
                    {
                        DateTime fechaLimite = fecha1Parse.AddDays(10); // Aumentar la fecha1 por 10 días
                        if (fechaLimite >= fecha2Parse)
                        {
                            fechaLimite = fecha2Parse;
                        }

                        string jsonParams = string.Format(@"{{
                         ""TipoExec"": {{
                             ""I_BEGDA"": ""{0}"",
                             ""I_ENDDA"": ""{1}"",
                             ""I_VKORG_B"": ""{2}""

                         }}
                     }}", fecha1Parse.ToString("yyyyMMdd"), fechaLimite.ToString("yyyyMMdd"), "1100"); //valor de VKORG POR DEFAULT


                        HttpResponseMessage response = await client.PostAsync(ApiZSD004, new StringContent(jsonParams, Encoding.UTF8, "application/json"));
                        if (response.IsSuccessStatusCode)
                        {
                            string responseBody = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrEmpty(responseBody))
                            {
                                Zsd004Response rootObject = JsonConvert.DeserializeObject<Zsd004Response>(responseBody);


                                if (rootObject != null && rootObject.tablaApi != null && rootObject.tablaApi.Length > 0)
                                {
                                    //DataTable dataTable = ConvertToDataTable(rootObject.tablaApi);
                                    DataTable dataTable = DataTableConverter.ConvertToDataTable(rootObject.tablaApi);

                                    string message = SqlConnector.publicDataTable(connectionString, dataTable, "[dbo].[RPT_ZSD004]"); //NUEVA TABLA
                                    Console.WriteLine("Inserción exitosa en la tabla [dbo].[RPT_ZSD004].");
                                }
                            }
                            else
                            {
                                Console.WriteLine("La respuesta de la API no contiene datos.");
                            }
                            await Task.Delay(5000);
                        }
                        else
                        {
                            Console.WriteLine("La solicitud no fue exitosa. Código de estado: " + response.StatusCode);
                        }
                        fecha1Parse = fechaLimite.AddDays(1); // Actualizar fecha1 para la siguiente iteración
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error al realizar la solicitud a la API: " + ex.Message);
                  
                }
            }

        }


        //// CJI3
            foreach (DataRow drow2 in dtFechas4.Rows)
            {
                // Obtener las fechas actuales y anteriores
                string anioActual = drow2[0].ToString();
                string mesActual = drow2[1].ToString();
                string diaActual = drow2[2].ToString();
                string anioRestado = drow2[3].ToString();
                string mesRestado = drow2[4].ToString();
                string diaRestado = drow2[5].ToString();

                // Construir las fechas en el formato requerido
                string fechaActual = anioActual + mesActual + diaActual;
                string fechaRestado = anioRestado + mesRestado + diaRestado;

                // Crear una lista de parámetros para la consulta SQL
                ArrayList parameters = new ArrayList();
                SqlParameter parameter;

                // Agregar las fechas como parámetros
                var fecha1 = anioRestado + '-' + mesRestado + '-' + diaRestado;
                var fecha2 = anioActual + '-' + mesActual + '-' + diaActual;

                parameter = new SqlParameter("@fecha1", SqlDbType.VarChar, 20);
                parameter.Value = fecha1;
                parameters.Add(parameter);

                parameter = new SqlParameter("@fecha2", SqlDbType.VarChar, 20);
                parameter.Value = fecha2;
                parameters.Add(parameter);

                // Eliminar los registros existentes en la base de datos usando la consulta SQL correspondiente
                DataTable dtElimina = SqlConnector.getEliminaDataTable(connectionString, "[dbo].[sap_depura_cji3]", parameters);

                // Crear una instancia del cliente HTTP
                using (HttpClient client = new HttpClient())
                {
                    foreach (DataRow drow in dtProyectos1.Rows)
                    {
                        try
                        {
                            // Establecer el encabezado Content-Type a application/json
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            // Crear los parámetros JSON para la solicitud
                            string proy = drow[1].ToString();

                            string jsonParams = $@"{{
                                         ""TipoExec"": {{
                                             ""I_VARIANT"": ""/VALIDA_MO"",
                                             ""I_BEGDATE"": ""{fechaRestado}"",
                                             ""I_ENDDATE"": ""{fechaActual}"",
                                             ""I_PSPID"": ""{proy}"",
                                             ""I_POSID"": """",
                                         }}
                                     }}";


                            // Enviar la solicitud POST a la API y obtener la respuesta
                            HttpResponseMessage response = await client.PostAsync(ApiCJI3, new StringContent(jsonParams, Encoding.UTF8, "application/json"));

                            // Verificar si la solicitud fue exitosa
                            if (response.IsSuccessStatusCode)
                            {
                                // Leer el contenido de la respuesta como una cadena
                                string responseBody = await response.Content.ReadAsStringAsync();

                                // Verificar si la respuesta contiene datos
                                if (!string.IsNullOrEmpty(responseBody))
                                {
                                    // Deserializar el JSON en un objeto RootObject
                                    CJI3Response rootObject = JsonConvert.DeserializeObject<CJI3Response>(responseBody);

                                    // Verificar si la propiedad tablaApi contiene datos
                                    if (rootObject != null && rootObject.tablaApi != null && rootObject.tablaApi.Length > 0)
                                    {
                                        // Convertir los datos en un DataTable
                                        DataTable dataTable = DataTableConverter.ConvertToDataTable(rootObject.tablaApi);

                                        // Insertar los datos en la base de datos usando la función SqlConnector.publicDataTable
                                        string message = SqlConnector.publicDataTable(connectionString, dataTable, "dbo.AGRI_RPT_CJI3");

                                        // Mostrar un mensaje de éxito
                                        Console.WriteLine("Los datos han sido insertados en la tabla dbo.AGRI_RPT_CJI3.");
                                    }

                                }
                                else
                                {
                                    // Mostrar un mensaje indicando que no hay datos en la respuesta
                                    Console.WriteLine("La respuesta de la API no contiene datos.");
                                }
                            }
                            else
                            {
                                // Mostrar un mensaje de error si la solicitud no fue exitosa
                                Console.WriteLine("Error al realizar la solicitud a la API: " + response.ReasonPhrase);
                            }
                        }
                        catch (Exception ex)
                        {
                            // Mostrar un mensaje de error si se produce una excepción al llamar a la API
                            Console.WriteLine("Excepción al llamar a la API: " + ex.Message);
                        }
                    }
                    Console.WriteLine("Se hizo con éxito");
                    Console.ReadLine();
                }
            }


        //IW38_KOB1
        foreach (DataRow drow2 in dtFechas5.Rows)
        {
            // Obtener las fechas actuales y anteriores
            string anioActual = drow2[0].ToString();
            string mesActual = drow2[1].ToString();
            string diaActual = drow2[2].ToString();
            string anioRestado = drow2[3].ToString();
            string mesRestado = drow2[4].ToString();
            string diaRestado = drow2[5].ToString();

            // Construir las fechas en el formato requerido
            string fechaActual = anioActual + mesActual + diaActual;
            string fechaRestado = anioRestado + mesRestado + diaRestado;

            // Crear una lista de parámetros para la consulta SQL
            ArrayList parameters = new ArrayList();
            SqlParameter parameter;

            // Agregar las fechas como parámetros
            var fecha1 = anioRestado + '-' + mesRestado + '-' + diaRestado;
            var fecha2 = anioActual + '-' + mesActual + '-' + diaActual;

            parameter = new SqlParameter("@fecha1", SqlDbType.VarChar, 20);
            parameter.Value = fecha1;
            parameters.Add(parameter);

            parameter = new SqlParameter("@fecha2", SqlDbType.VarChar, 20);
            parameter.Value = fecha2;
            parameters.Add(parameter);

            // Eliminar los registros existentes en la base de datos usando la consulta SQL correspondiente
            DataTable dtElimina = SqlConnector.getEliminaDataTable(connectionString, "[iw38_y_kob1].[sap_depura_iw38_y_kob1]", parameters);

            // Crear una instancia del cliente HTTP
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    // Establecer el encabezado Content-Type a application/json
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // Crear los parámetros JSON para la solicitud
                    string jsonParams = $@"{{
                            ""TipoExec"": {{
                            ""I_BEGDATE"": ""{fechaRestado}"",
                            ""I_ENDDATE"": ""{fechaActual}"",
                            }}
                        }}";

                    // Enviar la solicitud POST a la API y obtener la respuesta
                    HttpResponseMessage response = await client.PostAsync(ApiIW38_KOB1, new StringContent(jsonParams, Encoding.UTF8, "application/json"));

                    // Verificar si la solicitud fue exitosa
                    if (response.IsSuccessStatusCode)
                    {
                        // Leer el contenido de la respuesta como una cadena
                        string responseBody = await response.Content.ReadAsStringAsync();

                        // Verificar si la respuesta contiene datos
                        if (!string.IsNullOrEmpty(responseBody))
                        {
                            // Deserializar el JSON en un objeto RootObject
                            IW38_KOB1Response rootObject = JsonConvert.DeserializeObject<IW38_KOB1Response>(responseBody);

                            // Verificar si la propiedad tablaApi contiene datos
                            if (rootObject != null && rootObject.tablaApi != null && rootObject.tablaApi.Length > 0)
                            {
                                // Convertir los datos en un DataTable
                                DataTable dataTable = DataTableConverter.ConvertToDataTable(rootObject.tablaApi);

                                // Insertar los datos en la base de datos usando la función SqlConnector.publicDataTable
                                string message = SqlConnector.publicDataTable(connectionString, dataTable, "dbo.AGRI_RPT_IW38");

                                // Mostrar un mensaje de éxito
                                Console.WriteLine("Los datos han sido insertados en la tabla dbo.AGRI_RPT_IW38.");
                            }
                            else
                            {
                                // Mostrar un mensaje indicando que no hay datos en la respuesta
                                Console.WriteLine("La respuesta de la API no contiene datos.");
                            }
                            // Verificar si la propiedad tablaApi contiene datos
                            if (rootObject != null && rootObject.tablaApi2 != null && rootObject.tablaApi2.Length > 0)
                            {
                                // Convertir los datos en un DataTable
                                DataTable dataTable = DataTableConverter.ConvertToDataTable(rootObject.tablaApi2);

                                // Insertar los datos en la base de datos usando la función SqlConnector.publicDataTable
                                string message = SqlConnector.publicDataTable(connectionString, dataTable, "dbo.AGRI_RPT_KOB1");

                                // Mostrar un mensaje de éxito
                                Console.WriteLine("Los datos han sido insertados en la tabla dbo.AGRI_RPT_KOB1.");
                            }

                        }
                        else
                        {
                            // Mostrar un mensaje indicando que no hay datos en la respuesta
                            Console.WriteLine("La respuesta de la API no contiene datos.");
                        }
                    }
                    else
                    {
                        // Mostrar un mensaje de error si la solicitud no fue exitosa
                        Console.WriteLine("Error al realizar la solicitud a la API: " + response.ReasonPhrase);
                    }
                }
                catch (Exception ex)
                {
                    // Mostrar un mensaje de error si se produce una excepción al llamar a la API
                    Console.WriteLine("Excepción al llamar a la API: " + ex.Message);
                }

                Console.WriteLine("Se hizo con éxito");
                Console.ReadLine();
            }
        }

        Console.ReadLine();
    }

}


