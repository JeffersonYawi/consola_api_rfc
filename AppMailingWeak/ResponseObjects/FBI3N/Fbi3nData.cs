﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbl3n
{
    public class Fbi3nData
    {
        public string BUKRS { get; set; }
        public string NAME1 { get; set; }
        public string KONTO { get; set; }
        public string ZUONR { get; set; }
        public string XBLNR { get; set; }
        public string BELNR { get; set; }
        public string BLART { get; set; }
        public string BLDAT { get; set; }
        public string UMSKZ { get; set; }
        public string BUDAT { get; set; }
        public string ICO_DUE { get; set; }
        public string FAEDT { get; set; }
        public string WRSHB { get; set; }
        public string WAERS { get; set; }
        public string BWWRT { get; set; }
        public string HWAER { get; set; }
        public string AUGBL { get; set; }
        public string HKONT { get; set; }
        public string SGTXT { get; set; }
        public string BSTAT { get; set; }
        public string GJAHR { get; set; }
        public string BWWR2 { get; set; }
        public string HWAE2 { get; set; }
        public string REBZG { get; set; }
        public string BUTXT { get; set; }
        public string CONCATENACION { get; set; }
        public string CTA_BANC { get; set; }
        public string BANKA { get; set; }
        public string ANO_FC { get; set; }
        public string MES_FC { get; set; }
        public string COD { get; set; }
        public string SEMANA { get; set; }
        public string TIPO_INGRESO { get; set; }
        public string HBKID { get; set; }
        public string HKTID { get; set; }
        public string ZDMBE2 { get; set; }
        public string WAERS_USD { get; set; }
    }
}
