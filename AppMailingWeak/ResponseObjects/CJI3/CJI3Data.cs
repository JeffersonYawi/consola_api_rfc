﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbl3n
{
    public class CJI3Data
    {
        public string POSID { get; set; }
        public string KSTAR { get; set; }
        public string CEL_KTXT { get; set; }
        public string CEL_LTXT { get; set; }
        public string BUDAT { get; set; }
        public string MBGBTR { get; set; }
        public string WKGBTR { get; set; }
        public string WOGBTR { get; set; }
        public string WKFBTR { get; set; }
        public string PRTKO { get; set; }
        public string OBJNR_N1 { get; set; }
        public string MAT_TXT { get; set; }
        public string MEINH { get; set; }
        public string MATNR { get; set; }
        public string POBID { get; set; }
        public string GJAHR { get; set; }
        public string PERAB { get; set; }
    }
}
