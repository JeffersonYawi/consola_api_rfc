﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbl3n
{
    public class IW38_KOB1Data
    {
        public string ERDAT { get; set; }
        public string USTXT { get; set; }
        public string AUART { get; set; }
        public string AUFNR { get; set; }
        public string KTEXT { get; set; }
        public string ERNAM { get; set; }
        public string KOSTL { get; set; }
        public string EQUNR { get; set; }
        public string EQKTX { get; set; }
        public string TPLNR { get; set; }
        public string PLTXT { get; set; }
        public string STTXT { get; set; }
        public string GSTRP { get; set; }
        public string WERKS { get; set; }        
    }
    public class IW38_KOB1Data2
    {
        public string AUFNR { get; set; }
        public string OBJ_TXT { get; set; }
        public string KSTAR { get; set; }
        public string CEL_LTXT { get; set; }
        public string BUDAT { get; set; }
        public string MBGBTR { get; set; }
        public string WKGBTR { get; set; }
        public string KWAER { get; set; }
        public string WOGBTR { get; set; }
        public string OWAER { get; set; }
        public string WERKS { get; set; }
        public string MATNR { get; set; }
        public string MAT_TXT { get; set; }
        public string MEINB { get; set; }
        public string REFBN { get; set; }
        public string BELNR { get; set; }
        public string VRGNG { get; set; }
        public string STOKZ { get; set; }
        public string STFLG { get; set; }
        public string AWREF_REV { get; set; }
        public string BLTXT { get; set; }
        public string POBID { get; set; }
        public string POB_TXT { get; set; }
        public string POBART { get; set; }
        public string UOBID { get; set; }
        public string UOB_TXT { get; set; }
        public string EBTXT { get; set; }
        public string EBELN { get; set; }
        public string EBELP { get; set; }
        public string WRTTP { get; set; }
    }
}
