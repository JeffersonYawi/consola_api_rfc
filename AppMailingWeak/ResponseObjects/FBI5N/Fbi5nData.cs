﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbl3n
{
    public class Fbi5nData
    {
        public string BUKRS { get; set; }
        public string NAME1 { get; set; }
        public string KONTO { get; set; }
        public string ZUONR { get; set; }
        public string XBLNR { get; set; }
        public string BELNR { get; set; }
        public string BLART { get; set; }
        public string BLDAT { get; set; }
        public string UMSKZ { get; set; }
        public string BUDAT { get; set; }
        public string XDUEN { get; set; }
        public string FAEDT { get; set; }
        public string WRSHB { get; set; }
        public string WAERS { get; set; }
        public string BWWRT { get; set; }
        public string HWAER { get; set; }
        public string AUGBL { get; set; }
        public string HKONT { get; set; }
        public string SGTXT { get; set; }
        public string BSTAT { get; set; }
        public string GJAHR { get; set; }
        public string BWWR2 { get; set; }
        public string HWAE2 { get; set; }
        public string REBZG { get; set; }
        public string PRCTR { get; set; }
        public string LTEXT { get; set; }
        public string ZZ_TIPTRA { get; set; }
        public string ZZ_TIPTRA_TXT { get; set; }
        public string VBEL2 { get; set; }
        public string VBELN { get; set; }
        public string DIAS { get; set; }
        public string STATUS_VCTO { get; set; }
        public string STATUS { get; set; }
        public string ANO_FD { get; set; }
        public string MES_FD { get; set; }
        public string BUTXT { get; set; }
        public string COD { get; set; }
        public string TIPO_VENTA { get; set; }
        public string RESPONSABLE { get; set; }
        public string ANTICUAMIENTO { get; set; }
        public string VKGRP { get; set; }
        public string VENDEDOR { get; set; }
        public string ZDMBE2 { get; set; }
        public string WAERS_USD { get; set; }
    }
}
