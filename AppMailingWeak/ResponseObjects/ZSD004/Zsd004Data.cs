﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbl3n
{
    public class Zsd004Data
    {

        public string COD_EXPORT { get; set; }
        public string COD_CLIENTE { get; set; }
        public string VBELN_VA { get; set; }
        public string NAMEK_MP { get; set; }
        public string EXPORTADOR { get; set; }
        public string NOM_CLIENTE { get; set; }
        public string CONSIG { get; set; }
        public string TIPO_PRODUCTO { get; set; }
        public string VARIEDAD { get; set; }
        public string CATEGORIA { get; set; }
        public string TIPO_PRD { get; set; }
        public string MATNR { get; set; }
        public string ARKTX { get; set; }
        public string FORMATO { get; set; }
        public string CALIBRE { get; set; }
        public string CAJA { get; set; }
        public string ZZ_TIPTRA_TXT { get; set; }
        public string ZZ_MODEMB_TXT { get; set; }
        public string PAISDF { get; set; }
        public string LANDX { get; set; }
        public string ZZ_PUEEMB_TXT { get; set; }
        public string ZZ_PUEDES_TXT { get; set; }
        public string LFDAT { get; set; }
        public string ZZ_ETD { get; set; }
        public string ZZ_FECREALZARPE { get; set; }
        public string ZZ_ETA { get; set; }
        public string ZZ_FECREALARRIBO { get; set; }
        public string XBLNR_VF { get; set; }
        public string INCO1 { get; set; }
        public string ZZ_CODRESERVA { get; set; }
        public string ZZ_BL { get; set; }
        public string ZZ_NUMFCL { get; set; }
        public string NOM_OPERADOR { get; set; }
        public string TOTAL_CAJAS { get; set; }
        public string KILOS_EXPORTADOS { get; set; }
        public string KILOS_LIQ { get; set; }
        public string ZZ_MODALIDAD_VENTA_TXT { get; set; }
        public string Z_EST_LIQ { get; set; }
        public string FACT_USD { get; set; }
        public string FACT_FOB { get; set; }
        public string MONTO_LIQ_USD { get; set; }
        public string MONTO_FOB_LIQ_USD { get; set; }
        public string MONTO_FLETE { get; set; }
        public string MONTO_NC_ND { get; set; }
        public string CHARG { get; set; }
        public string FEC_LIQUI { get; set; }

        //public string SEMANA_EMB { get; set; }
        //public string SEMANA_DESP { get; set; }
        //public string MES_DESP { get; set; }
        //public string ZZ_TIPTRA { get; set; }
        //public string ZZ_TIPTRA_TXT { get; set; }
        //public string LFDAT { get; set; }
        //public string VKORG { get; set; }
        //public string EXPORTADOR { get; set; }
        //public string KUNNR { get; set; }
        //public string NOM_CLIENTE { get; set; }
        //public string ZZ_ETA { get; set; }
        //public string ZZ_ETD { get; set; }
        //public string ZZ_FECREALARRIBO { get; set; }
        //public string ZZ_FECREALZARPE { get; set; }
        //public string LAND1 { get; set; }
        //public string LANDX { get; set; }
        //public string ZZ_NUMFCL { get; set; }
        //public string WERKS { get; set; }
        //public string VBELN_VA { get; set; }
        //public string POSNR_VA { get; set; }
        //public string XBLNR_VF { get; set; }
        //public string WAERK { get; set; }
        //public string INCO1 { get; set; }
        //public string ZZ_PUEDES_TXT { get; set; }
        //public string ZZ_PUEEMB_TXT { get; set; }
        //public string VARIEDAD { get; set; }
        //public string CALIBRE { get; set; }
        //public string CAJA { get; set; }
        //public string CATEGORIA { get; set; }
        //public string FORMATO { get; set; }
        //public string TIPO_PRD { get; set; }
        //public string MATNR { get; set; }
        //public string ARKTX { get; set; }
        //public string CONSIG { get; set; }
        //public string NOM_CONSIG { get; set; }
        //public string ESTADO_DESPACHO { get; set; }
        //public string ESTADO_LIQ { get; set; }
        //public string TIPO_PRODUCTO { get; set; }
        //public string XBLNR_VL { get; set; }
        //public string PLANTA_EMPAQUE { get; set; }
        //public string VBELN_VL { get; set; }
        //public string POSNR_VL { get; set; }
        //public string VBELN_VF { get; set; }
        //public string POSNR_VF { get; set; }
        //public string ZZ_CODRESERVA { get; set; }
        //public string ZZ_BL { get; set; }
        //public string OPERADOR { get; set; }
        //public string NOM_OPERADOR { get; set; }
        //public string NAVIERA { get; set; }
        //public string NOM_NAVIERA { get; set; }
        //public string ZZ_NAVE_VUELO { get; set; }
        //public string KCBRGEW { get; set; }
        //public string PESO_NETO { get; set; }
        //public string TOTAL_CAJAS { get; set; }
        //public string KILOS_EXPORTADOS { get; set; }
        //public string FACT { get; set; }
        //public string FACT_USD { get; set; }
        //public string FACT_FOB { get; set; }
        //public string KG_FACT_FOB { get; set; }
        //public string KILOS_LIQ { get; set; }
        //public string MONTO_LIQ { get; set; }
        //public string MONTO_LIQ_USD { get; set; }
        //public string MONTO_FOB_LIQ { get; set; }
        //public string MONTO_FOB_LIQ_USD { get; set; }
        //public string KG_FOB_LIQ_USD { get; set; }
        //public string MONTO_NC_ND { get; set; }
        //public string MONTO_SEGURO { get; set; }
        //public string MONTO_FLETE { get; set; }
        //public string OTROS_GASTOS_MO { get; set; }
        //public string OTROS_GASTOS_CU { get; set; }
        //public string OTROS_GASTOS { get; set; }
        //public string MONTO_GASTOS_DESTINO { get; set; }
        //public string COMISION { get; set; }
        //public string FACT_NETO { get; set; }
        //public string ZZ_MODEMB { get; set; }
        //public string ZZ_MODEMB_TXT { get; set; }
        //public string SKTOF { get; set; }
        //public string PAISDF { get; set; }
        //public string ZZ_CONTINENTE { get; set; }
        //public string Z_EST_LIQ { get; set; }
        //public string VBELN { get; set; }
        //public string ZZ_SEMETA { get; set; }
        //public string ZZ_SEMETA_REAL { get; set; }
        //public string ZZ_SEMETD { get; set; }
        //public string ZZ_SEMETD_REAL { get; set; }
        //public string MES_ETD_REAL { get; set; }
        //public string MESARRIBO { get; set; }
        //public string KURSF_EUR_USD { get; set; }
        //public string ZZ_CAMPAIN { get; set; }
        //public string NOM_ETIQUETA { get; set; }
        //public string ZZ_MODALIDAD_VENTA_TXT { get; set; }
        //public string LIFNR_MP { get; set; }
        //public string NAMEK_MP { get; set; }
        //public string FLAG_DEV { get; set; }
        //public string CHARG { get; set; }
    }
}
