﻿using SAP.Middleware.Connector;

using System;
using System.Data;
using Infrastructure.Utils;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;



namespace AgrisysLiquidacionesCorreo
{
    public class RfcConnector : IDestinationConfiguration
    {
        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public bool ChangeEventsSupported()
        {
            return false;
        }

        public RfcConfigParameters GetParameters(string destinationName)
        {
            if ("NCO_TESTS".Equals(destinationName))
            {
                //CONEXION A QAS
                /*RfcConfigParameters parms = new RfcConfigParameters();                
                parms.Add(RfcConfigParameters.AppServerHost, "10.45.4.163");
                parms.Add(RfcConfigParameters.SystemNumber, "01");
                parms.Add(RfcConfigParameters.SystemID, "");
                parms.Add(RfcConfigParameters.Client, "200");
                parms.Add(RfcConfigParameters.SAPRouter, "");
                parms.Add(RfcConfigParameters.Language, "ES");
                parms.Add(RfcConfigParameters.Name, "NCO_TESTS");
                //parms.Add(RfcConfigParameters.ConnectionIdleTimeout, "");
                parms.Add(RfcConfigParameters.MaxPoolSize, "40");
                parms.Add(RfcConfigParameters.PoolSize, "30");
                parms.Add(RfcConfigParameters.User, "SOPORTE");
                parms.Add(RfcConfigParameters.Password, "Grti2021**+");*/

                //CONEXION A PRODUCCIÓN
                RfcConfigParameters parms = new RfcConfigParameters();
                parms.Add(RfcConfigParameters.AppServerHost, "10.45.1.154");
                parms.Add(RfcConfigParameters.SystemNumber, "04");
                parms.Add(RfcConfigParameters.SystemID, "");
                parms.Add(RfcConfigParameters.Client, "300");
                parms.Add(RfcConfigParameters.SAPRouter, "");
                parms.Add(RfcConfigParameters.Language, "ES");
                parms.Add(RfcConfigParameters.Name, "NCO_TESTS");
                //parms.Add(RfcConfigParameters.ConnectionIdleTimeout, "");
                parms.Add(RfcConfigParameters.MaxPoolSize, "40");
                parms.Add(RfcConfigParameters.PoolSize, "30");

                //Consulta Credenciales 
                string connectionString = ConfigurationManager.ConnectionStrings["conexionbd"].ConnectionString;
                DataTable dtCredenciales = SqlConnector.getConsultaDataTable(connectionString, "dbo.lista_credenciales");
                parms.Add(RfcConfigParameters.User, "SOPORTE");

                foreach (DataRow drow in dtCredenciales.Rows)
                {
                    string password = drow[0].ToString();
                    parms.Add(RfcConfigParameters.Password, password);
                }
                return parms;
            }
            else
            {
                return null;
            }
        }
    }
}
