﻿namespace AgrisysLiquidacionesCorreo
{
    public class OutResultData<T>
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public T data { get; set; }

        public OutResultData()
        {
            statusCode = 0;
            message = "";
        }
    }
}
