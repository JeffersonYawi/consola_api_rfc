﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

public static class DataTableConverter
{
    /// <summary>
    /// Convierte un arreglo de objetos en un objeto DataTable.
    /// </summary>
    /// <typeparam name="T">Tipo de objeto en el arreglo.</typeparam>
    /// <param name="dataArray">Arreglo de objetos.</param>
    /// <returns>Objeto DataTable con los datos del arreglo de objetos.</returns>
    public static DataTable ConvertToDataTable<T>(T[] dataArray)
    {
        if (dataArray == null || dataArray.Length == 0)
        {
            return null;
        }

        DataTable dataTable = new DataTable();
        PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

        // Agregar columnas al DataTable basado en las propiedades del tipo T
        foreach (PropertyInfo prop in properties)
        {
            dataTable.Columns.Add(prop.Name, prop.PropertyType);
        }

        // Agregar filas al DataTable con los valores de las propiedades de los objetos en el arreglo
        foreach (T data in dataArray)
        {
            DataRow row = dataTable.NewRow();

            foreach (PropertyInfo prop in properties)
            {
                row[prop.Name] = prop.GetValue(data);
            }

            dataTable.Rows.Add(row);
        }

        return dataTable;
    }
}
