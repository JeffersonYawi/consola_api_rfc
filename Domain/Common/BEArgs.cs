﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Common
{
    public class BEArgs : BEMaster
    {
        public int estadoID { get; set; }
        public List<int> codigos { get; set; }
        public int usuarioID { get; set; }
    }
}
