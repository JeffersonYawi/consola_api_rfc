﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Common
{
    public class BEMaster
    {
        public int id { get; set; }
        public int accion { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public string login { get; set; }
        public string host { get; set; }
        public string version { get; set; }
        public bool isActivated { get; set; }
        public string tokenFCM { get; set; }
    }
}
