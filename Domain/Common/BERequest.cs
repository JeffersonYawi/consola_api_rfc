﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Common
{
    public class BERequest
    {
        public string code { get; set; }
        public string type { get; set; }
        public string message { get; set; }
        public string status { get; set; }
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string date_cr { get; set; }
        public string date_up { get; set; }
        public string host { get; set; }
        public bool is_read { get; set; }
    }
}
